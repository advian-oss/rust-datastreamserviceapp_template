#!/bin/bash -x
test -t 1 && USE_TTY="-it"
set -e
old_workdir=$(pwd)
tmp_dir=$(mktemp -d "/tmp/ccrusttestbuild.XXXXXXXX")
cd $tmp_dir
cookiecutter -f --no-input $old_workdir
cd rust-nameless

# Init repo so it looks like a real project
git init
git checkout -b cctest
git add .
git commit -am 'automated tests'

if [ $(uname -s ) == "Darwin" ]; then
  export DOCKER_SSHAGENT="-v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"
else
  export DOCKER_SSHAGENT="-v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK"
fi

# We need buildkit
export DOCKER_BUILDKIT=1
# Docker tests
docker build --progress plain --ssh default --target test -t ccrusttestbuild:test  .
docker run $USE_TTY --rm --security-opt seccomp=unconfined `echo $DOCKER_SSHAGENT` ccrusttestbuild:test
# Docker devel shell
docker build --progress plain --ssh default --target devel_shell -t ccrusttestbuild:devel_shell .
docker run $USE_TTY --rm -v `pwd`:/app `echo $DOCKER_SSHAGENT` ccrusttestbuild:devel_shell -c "cargo update"
docker run $USE_TTY --rm -v `pwd`:/app `echo $DOCKER_SSHAGENT` ccrusttestbuild:devel_shell -c "SKIP=check-executables-have-shebangs pre-commit run --all-files"
docker run $USE_TTY --rm -v `pwd`:/app `echo $DOCKER_SSHAGENT` ccrusttestbuild:devel_shell -c "which git-up"
# Docker production image
docker build --progress plain --ssh default --target production -t ccrusttestbuild:production .
docker run $USE_TTY --rm ccrusttestbuild:production true

rm -rf $tmp_dir
