use datastreamcorelib::datamessage::PubSubDataMessage;
use datastreamcorelib::pubsub::{PubSubMessage, Subscription};
use datastreamservicelib::utils::send_task;
use datastreamservicelib::TerminationFlag;
use failure::Fallible;
use log;
use parking_lot::Mutex;
use std::convert::TryFrom;
use std::sync::atomic::Ordering;
use std::sync::Arc;
use std::time::Duration;
use tokio;
use tokio::time::delay_for;

/// Safe mutex pointer to a MsgReceiver instance
pub type MsgReceiverArc = Arc<Mutex<MsgReceiver>>;

/// Receives messages and displays them via log
#[derive(Default, Debug)]
pub struct MsgReceiver {
    pub counter: i64,
}

impl MsgReceiver {
    /// Return fresh instance
    pub fn new() -> MsgReceiver {
        MsgReceiver { counter: 0 }
    }
}

/// Message receive callback, decode and show on log
pub fn msg_callback(recv: MsgReceiverArc, _sub: &Subscription, msg: PubSubMessage) {
    let msg = match PubSubDataMessage::try_from(msg) {
        Err(e) => {
            log::error!("Decode error {:?}", e);
            return;
        }
        Ok(msg) => msg,
    };
    log::debug!("Got message {:?} on sub {:?}", &msg, &_sub);
    // Scope-limit the lock
    {
        let mut recv_locked = recv.lock();
        recv_locked.counter = recv_locked.counter + 1
    }
    let mut new_msg = msg.clone();
    new_msg.topic = "newtopic".to_string();
    // We need to spawn a task to send the message since the manager is locked right now...
    log::info!("Spawning re-publish for {:?}", &new_msg);
    tokio::spawn(send_task(new_msg));
}

/// Reports count of messages every second
pub async fn msg_count_reporter(recv: MsgReceiverArc, term: TerminationFlag) -> Fallible<()> {
    loop {
        if term.load(Ordering::Relaxed) {
            log::trace!("Got term flag, exiting count reporter task");
            return Ok(());
        }
        delay_for(Duration::from_millis(1000)).await;
        log::info!("Have received {} messages so far", &recv.lock().counter);
    }
}
