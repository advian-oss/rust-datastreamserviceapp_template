pub mod defaultconfig;
mod receiver;
use datastreamcorelib::datamessage::PubSubDataMessage;
use datastreamcorelib::pubsub::{PubSubMessage, Subscription};
use datastreamservicelib::heartbeat::heartbeat_task;
use datastreamservicelib::utils::{configval_as_string_vec, wait_for_tasks};
use datastreamservicelib::zmqwrappers::TokioPubSubManager;
use datastreamservicelib::TerminationFlag;
// we need to explicitly import this trait for it to work
use datastreamcorelib::pubsub::PubSubManager;
use failure::Fallible;
use futures::stream::FuturesUnordered;
use log;
use parking_lot::Mutex;
use serde_json;
use std::sync::atomic::Ordering;
use std::sync::Arc;
use std::time::Duration;
use tokio::time::delay_for;
use toml;

/// Main entrypoint
pub async fn mainloop(config: toml::Value, term: TerminationFlag) -> Fallible<()> {
    // Instances of things we are going to need
    let psmgr_mutex = TokioPubSubManager::instance();
    let recv: receiver::MsgReceiverArc = Arc::new(Mutex::new(receiver::MsgReceiver::new()));

    // Scope-limit the lock
    {
        let mut psmgr_locked = psmgr_mutex.lock();
        psmgr_locked.term_flag = term.clone();
        psmgr_locked
            .set_default_pub_uris(&configval_as_string_vec(&config["zmq"]["pub_sockets"])?)?;

        let recv_cb_clone = recv.clone();
        // Subscribe to our own heartbeat
        let sub = Subscription::new(
            vec![configval_as_string_vec(&config["zmq"]["pub_sockets"])?[0].clone()],
            vec!["HEARTBEAT".to_string()],
            move |sub: &Subscription, msg: PubSubMessage| {
                receiver::msg_callback(recv_cb_clone.clone(), &sub, msg);
            },
        )?;
        psmgr_locked.subscribe(&sub)?;
    }
    // Give the socket a moment to stabilize
    delay_for(Duration::from_millis(150)).await;

    // Create tasks
    let tasks = FuturesUnordered::new();
    tasks.push(tokio::spawn(heartbeat_task(term.clone())));
    tasks.push(tokio::spawn(publisher_task(
        "count_to_ten".to_string(),
        10,
        term.clone(),
    )));
    tasks.push(tokio::spawn(publisher_task(
        "counter".to_string(),
        -1,
        term.clone(),
    )));
    tasks.push(tokio::spawn(receiver::msg_count_reporter(
        recv.clone(),
        term.clone(),
    )));

    wait_for_tasks(tasks).await?;
    Ok(())
}

/// Publisher task, will publish messages with counter in the given topic
async fn publisher_task(topic: String, count_to: i64, term: TerminationFlag) -> Fallible<()> {
    let psmgr = TokioPubSubManager::instance();
    let mut msgno: i64 = 0;
    loop {
        msgno = msgno + 1;
        let mut msg = PubSubDataMessage::new(topic.clone())?;
        msg.data["msgno"] = serde_json::to_value(msgno)?;
        psmgr.lock().publish(&msg)?;
        if count_to > 0 && msgno >= count_to {
            log::info!("Sent {} messages, stopping now", msgno);
            return Ok(());
        }
        if term.load(Ordering::Relaxed) {
            log::trace!("Got term flag, exiting publisher task");
            return Ok(());
        }
        delay_for(Duration::from_millis(500)).await;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::defaultconfig::default_config_str;
    use datastreamcorelib::logging::init_logging;
    use std::env::temp_dir;
    use std::sync::atomic::AtomicBool;
    use std::sync::Arc;
    use toml;

    #[tokio::test]
    async fn mainloop_heartbeat() {
        init_logging(log::LevelFilter::Trace).unwrap();
        let term: TerminationFlag = Arc::new(AtomicBool::new(false));
        let mut config: toml::Value = toml::from_str(default_config_str().as_str()).unwrap();
        let mut tmppath1 = temp_dir();
        tmppath1.push("3f7639c2-df18-49fa-a788-11a0486e7ac1_pub.sock");
        let sockpath1 = "ipc://".to_string() + &tmppath1.to_string_lossy();
        let new_toml_sockets = toml::Value::try_from(vec![sockpath1.clone()]).unwrap();
        config["zmq"]["pub_sockets"] = new_toml_sockets;
        assert_eq!(
            configval_as_string_vec(&config["zmq"]["pub_sockets"]).unwrap(),
            vec![sockpath1.clone()]
        );
        let maintask = tokio::spawn(mainloop(config, term.clone()));

        delay_for(Duration::from_millis(1000)).await;

        // Tell things to shut down
        term.store(true, Ordering::Relaxed);
        maintask.await.expect("Main task failed").unwrap();
    }
}
