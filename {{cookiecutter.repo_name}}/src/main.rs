///! {{ cookiecutter.project_short_description }}
use clap::{crate_authors, crate_version, App, Arg};
use datastreamcorelib::logging::init_logging_from_verbosity;
use datastreamservicelib::utils::{parse_config_file, setup_signals_termination};
use failure::Fallible;
use {{ cookiecutter.package_name }}::defaultconfig::default_config_str;
use {{ cookiecutter.package_name }}::mainloop;
use std::path::Path;
use tokio::runtime;

/// {{ cookiecutter.project_short_description }}
fn main() -> Fallible<()> {
    let app = App::new("{{ cookiecutter.package_name }}")
        .about({{ cookiecutter.project_short_description|tojson }})
        .version(crate_version!())
        .author(crate_authors!())
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Increases logging verbosity each use for up to 3 times"),
        )
        .arg(
            Arg::with_name("defaultconfig")
                .long("defaultconfig")
                .help("Dump default config to stdout"),
        )
        .arg(
            Arg::with_name("configfile")
                .takes_value(true)
                .index(1)
                .empty_values(false)
                .required_unless("defaultconfig")
                .help("Config file path"),
        );
    let app_args = app.get_matches();
    if app_args.is_present("defaultconfig") {
        println!("{}", default_config_str());
        return Ok(());
    }

    let verbosity: u64 = app_args.occurrences_of("verbose");
    init_logging_from_verbosity(verbosity)?;

    let configpath = Path::new(app_args.value_of("configfile").unwrap());
    let parsed_config = parse_config_file(configpath)?;
    let term = setup_signals_termination()?;

    // Use the non-threaded scheduler explicitly
    let mut rt = runtime::Builder::new()
        .basic_scheduler()
        .enable_time()
        .enable_io()
        .build()?;
    rt.block_on(mainloop(parsed_config, term))?;
    Ok(())
}
