/// Return default configuration as string
pub fn default_config_str() -> String {
    return String::from(
        r###"
[zmq]
pub_sockets = ["ipc:///tmp/{{cookiecutter.package_name}}_pub.sock", "tcp://*:{{ range(49152, 65535) | random }}"]
"###,
    )
    .trim()
    .to_string();
}
