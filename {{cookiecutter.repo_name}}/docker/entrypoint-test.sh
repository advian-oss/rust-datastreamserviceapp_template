#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  cargo install cargo2junit cargo-tarpaulin
  cargo test -- -Z unstable-options --format json | cargo2junit > junit.xml
  coverage_report=$(mktemp)
  cargo tarpaulin --verbose --frozen 2>&1 | cat > $coverage_report
  cat $coverage_report
  grep '% coverage, ' $coverage_report | cut -d\  -f1 | xargs echo "Total coverage:"
else
  exec "$@"
fi
