#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  exec {{cookiecutter.package_name}} -vv docker_config.toml
else
  exec "$@"
fi
