# {{cookiecutter.project_name}}

{{ cookiecutter.project_short_description }}

## Docker

For more controlled deployments and to get rid of "works on my computer" -syndrome, we always
make sure our software works under docker.

It's also a quick way to get started with a standard development environment.

### SSH agent forwarding

We need [buildkit][buildkit]

    export DOCKER_BUILDKIT=1

[buildkit]: https://docs.docker.com/develop/develop-images/build_enhancements/

And also the exact way for forwarding agent to running instance is different on OSX

    export DOCKER_SSHAGENT="-v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"

and Linux

    export DOCKER_SSHAGENT="-v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK"

### Creating a development container

Build image, create container and start it (switch the 1234 port to the port from src/defaultconfig.rs)

    docker build --ssh default --target devel_shell -t {{cookiecutter.package_name}}:devel_shell .
    docker create --name {{cookiecutter.package_name}}_devel -p 1234:1234 -v `pwd`":/app" -it -v /tmp:/tmp `echo $DOCKER_SSHAGENT` {{cookiecutter.package_name}}:devel_shell
    docker start -i {{cookiecutter.package_name}}_devel

### pre-commit considerations

If working in Docker instead of native env you need to run the pre-commit checks in docker too

    docker exec -i {{cookiecutter.package_name}}_devel /bin/bash -c "pre-commit install"
    docker exec -i {{cookiecutter.package_name}}_devel /bin/bash -c "pre-commit run --all-files"

You need to have the container running, see above. Or alternatively use the docker run syntax but using
the running container is faster

    docker run --rm -v `pwd`":/app" {{cookiecutter.package_name}}:devel_shell -c "pre-commit run --all-files"

### Test suite

You can use the devel shell to run py.test when doing development, for CI use
the "test" target in the Dockerfile

    docker build --ssh default --target test -t {{cookiecutter.package_name}}:test .
    docker run --rm --security-opt seccomp=unconfined -v `pwd`":/app" `echo $DOCKER_SSHAGENT` {{cookiecutter.package_name}}:test

### Production docker

There's a "production" target as well for running the application (change the "1234" port and "myconfig.toml" for
config file) and remember to change that architecture tag to arm64 if building on ARM:

    docker build --ssh default --target production -t {{cookiecutter.package_name}}:latest .
    docker run --name {{cookiecutter.package_name}} -v myconfig.toml:/app/docker_config.toml -p 1234:1234 -it -v /tmp:/tmp `echo $DOCKER_SSHAGENT` {{cookiecutter.package_name}}:amd64-latest


## Local Development

TODO: Remove the repo init from this document after you have done it.

TLDR:

  - Init your repo (first create it on-line and make note of the remote URI)

        git init
        git add .
        git commit -m 'Cookiecutter stubs'
        git remote add origin MYREPOURI
        git push origin master

  - change to a branch

        git checkout -b my_branch

  - Make sure you have pre-commit & bump2version installed: pip3 install pre-commit bump2version
  - Make sure you have cargo installed: https://doc.rust-lang.org/cargo/getting-started/installation.html
  - Install project deps and pre-commit hooks

        cargo build
        pre-commit install
        pre-commit run --all-files

  - Ready to go, try the following

        cargo run --bin {{cookiecutter.package_name}} -- --defaultconfig >config.toml
        cargo run --bin {{cookiecutter.package_name}} -- -vvv config.toml

Running "pre-commit run --all-files" and "cargo test" regularly during development and
especially before committing will save you some headache.
